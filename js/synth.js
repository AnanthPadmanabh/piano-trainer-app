// Tone JS
var piano = new Tone.Sampler({
                           "C4" : "https://rawgit.com/AnanthPadmanabh/Piano-Trainer-App/master/Audio/C4.mp3",      // Evan Mazunik (2017) Piano.mf.F#4. University of Iowa Electronic Music Studios [download] [Accessed 15 April 2018]
                           "F#4" : "https://rawgit.com/AnanthPadmanabh/Piano-Trainer-App/master/Audio/F%234.mp3",  // Evan Mazunik (2017) Piano.mf.C5. University of Iowa Electronic Music Studios [download] [Accessed 15 April 2018]
                           "C5" : "https://rawgit.com/AnanthPadmanabh/Piano-Trainer-App/master/Audio/C5.mp3",      // Evan Mazunik (2017) Piano.mf.F#5. University of Iowa Electronic Music Studios [download] [Accessed 15 April 2018]
                           "F#5" : "https://rawgit.com/AnanthPadmanabh/Piano-Trainer-App/master/Audio/F%235.mp3"   // Evan Mazunik (2017) Piano.mf.C4. University of Iowa Electronic Music Studios [download] [Accessed 15 April 2018]
                           });
piano.connect(Tone.Master);

// DOM
var blink = document.getElementById('buttons');
var sqr = document.getElementById('blueSquare');
var txt = document.getElementById('filltext');
var notedisp = document.getElementById('notedisplay');
var difficulty = document.getElementById('difficulty');

var refresh = "yes";

// Event listeners for random note square
sqr.addEventListener("mousedown",function(event) {
                     randButtons(event, refresh);
                     },true);
sqr.addEventListener("mouseup",function(event) {
                     randButtons(event, refresh);
                     },true);

// NexusUI
var keyboard = new Nexus.Piano("#pianokeys",{
                            'size': [1176,400],
                            lowNote: 60,
                            highNote: 60 + 12
                            });

// Map keys to corresponding note names
var noteMap = {};
var noteNumberMap = [];
var notes = [ "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" ];

var rand = "C4";

var size = 12;

var randomNote = 60;

// March through notes to assign note numbers to corresponding note names
for(var i = 60; i < 60 + size; i++) {
    
    key = notes[i % 12];
    octave = ((i / 12) | 0) - 1; // MIDI octave
    
    key += octave;
    
    noteMap[key] = i;
    noteNumberMap[i] = key;
    
}



// Random note button listener
function randButtons(event, ref) {
    
    if(event.type == 'mousedown')
    {
        var randNote = Math.floor(Math.random() * size + 60);
        
        // Whether to refresh or not
        if(ref == "yes")
            
            randomNote = randNote;
            
        var color;
        
        // Change colour of square according to random key for hint.
        
        if(randomNote < 65 && randomNote % 2 == 0 || randomNote > 64 && randomNote % 2 != 0 )
        {
            color = "white";
            txt.style.color = notedisp.style.color = difficulty.style.color = "black";
        }
        else if(randomNote < 65 && randomNote % 2 != 0 || randomNote > 64 && randomNote % 2 === 0 )
        {
            color = "black";
            txt.style.color = notedisp.style.color = difficulty.style.color = "white";
        }
        
        if(difficulty.selectedIndex == "0")
            sqr.style.background = color;
        else if(difficulty.selectedIndex == "1")
        {
            sqr.style.background = "royalblue";
            txt.style.color = notedisp.style.color = difficulty.style.color = "black";
        }
            
        console.log("refresh" + " " + ref);
        var randNoteName = noteNumberMap[randomNote];
        
        console.log("Random note  " + randomNote);
        
        blink.style.background ="orchid";
        
        rand = randNoteName;
        
        piano.triggerAttack(randNoteName);
        
    }
    else
    {
        piano.triggerRelease(randNoteName);
        
        txt.innerHTML = "Guess Me";
        notedisp.innerHTML = "";
        

    }
}

// Nexus keyboard function
keyboard.on('change', function(keydata) {
            
    
    if (keydata.state) { // note pressed
            
        var note = keydata.note;
        
        var noteName = noteNumberMap[note];
         
        piano.triggerAttack(noteName);
            
        console.log(noteName + " " + rand);
            
            // Mechanism for showing right or wrong as green or red.
            
            if(noteName == rand)
            {
            blink.style.background ="green";
            refresh = "yes";
            txt.innerHTML = "Press Me";
            notedisp.innerHTML = noteName;
            }
            
            else if(noteName != rand)
            {
            blink.style.background ="red";
            refresh = "no";
            }
            
    }
    else { // note released
            
           piano.triggerRelease(noteName);

    }
            });
